const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Product = require("../models/Product.js")

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		fName : reqBody.fName,
		lName : reqBody.lName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
		// 10 = salt
	})
	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				//Generate an access
				// return  {access : auth.createAccessToken(result)}
				return {
					access: auth.createAccessToken(result),
					id: result.id,
					isAdmin: result.isAdmin
				}
					
			}
		}
	})
}
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		return result;
	})
}

module.exports.getAllUsers = (data) => {
	console.log(data.isAdmin)

	if(data.isAdmin) {
		return User.find({}).then(result => {
			return result
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve('User must be ADMIN to access this.')

	return message.then((value) => {
		return value
	})
}

module.exports.order = async (data) => {

	//Adds the productId in the user's enrollment array
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.productInfo.push({productId : data.productId});

		return user.save().then((user, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})

	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.userInfo.push({userId : data.userId});

		return product.save().then((product, error) => {
			if(error){
				return false
			}else{
				return true;
			}
		})
	})

	if(isUserUpdated && isProductUpdated){
		//isUserUpdated = true
		//isProductUpdatted = true
		//final output = TRUE
		return true;
	}else{
		//If one of these: isUserUpdated or isProductUpdated is false
		//Output will be false

		//If both isUserUpdated and isProductUpdated
		//Output will be a concrete FALSE
		return false;
	}
}